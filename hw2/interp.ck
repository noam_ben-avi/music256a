// an ADSR envelope
// (also see envelope.ck)
Noise s => ADSR e => dac;

// set a, d, s, and r
e.set( 8000::ms, 800::ms, .1, 4000::ms );
// set gain
.5 => s.gain;


// spawn parallel shred
spork ~ interp();

// infinite time-loop
while( true )
{

    // key on - start attack
    e.keyOn();
    // advance time by 800 ms
    8000::ms => now;
    // key off - start release
    e.keyOff();
    // advance time by 800 ms
    4000::ms => now;
}

// interpolator
fun void interp()
{
    SinOsc osc1 => dac;
}
