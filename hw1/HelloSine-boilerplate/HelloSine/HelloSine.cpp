//-----------------------------------------------------------------------------
// name: HelloSine.cpp
// desc: hello sine wave, real-time
//
// author: Ge Wang (ge@ccrma.stanford.edu)
//   date: fall 2017
//   uses: RtAudio by Gary Scavone
//-----------------------------------------------------------------------------
#include "RtAudio.h"
#include <math.h>
#include <iostream>
#include <cstdlib>
#include <string>
using namespace std;


// our datetype
#define SAMPLE float
// corresponding format for RtAudio
#define MY_FORMAT RTAUDIO_FLOAT32
// sample rate
const long MY_SRATE = 44100;
// number of channels
const long MY_CHANNELS = 2;
// frame size
const long MY_FRAMESIZE = 512;

int timeStep  = 0;

int TYPE      = -1;
int FREQUENCY = 0;
float WIDTH   = 0;



//-----------------------------------------------------------------------------
// name: callme()
// desc: audio callback
//-----------------------------------------------------------------------------
int callme( void * outputBuffer, void * inputBuffer, unsigned int numFrames,
            double streamTime, RtAudioStreamStatus status, void * data )
{
    // debug print something out per callback
    float pi = 3.14;
    int fc = 440;
    long fs = MY_SRATE;

    SAMPLE * out = (SAMPLE *) outputBuffer;
    SAMPLE * in = (SAMPLE *) inputBuffer;

    // sin wave
    if(TYPE == 0){
      int currFrame = 0;
      for(int i = 0; i < MY_FRAMESIZE; i++){
        timeStep++;
        for(int j = 0; j < MY_CHANNELS; j++){
          out[currFrame] = sin( 2 * pi * FREQUENCY * timeStep / fs );
          currFrame++;
        }
      }
    }
    return 0;
}




//-----------------------------------------------------------------------------
// name: main()
// desc: entry point
//-----------------------------------------------------------------------------
int main( int argc, char ** argv )
{
    // instantiate RtAudio object
    RtAudio adac;
    // variables
    unsigned int bufferBytes = 0;
    // frame size
    unsigned int bufferFrames = MY_FRAMESIZE;


    if (argc == 4){
      if(strcmp(argv[1],"--sine")){
        TYPE      = 0;
        cerr << "Sin Wav ";
      }
      FREQUENCY = atoi( argv[2] );
      WIDTH     = atof( argv[3] );


    }else{
      cerr << "usage:";
      cerr << "sig-gen [type] [frequency] [width] \n";
      cerr << "        [type]: --sine |  --noise | --impulse | --pulse \n";
      cerr << "        [frequency]: (a number > 0, not applicable to noise) \n";
      cerr << "        [width]: pulse width (only applicable to pulse) \n";
    }
    cerr << "this is amount of arguments: ";
    cerr << argc;
    cerr << argv[0];
    // NOTE: check arguments...
    // cerr << argv[0];
    // cerr << argv[1];
    // cerr << argv[2];

    // check for audio devices
    if( adac.getDeviceCount() < 1 )
    {
        // nopes
        cout << "no audio devices found!" << endl;
        exit( 1 );
    }

    // let RtAudio print messages to stderr.
    adac.showWarnings( true );

    // set input and output parameters
    RtAudio::StreamParameters iParams, oParams;
    iParams.deviceId = adac.getDefaultInputDevice();
    iParams.nChannels = MY_CHANNELS;
    iParams.firstChannel = 0;
    oParams.deviceId = adac.getDefaultOutputDevice();
    oParams.nChannels = MY_CHANNELS;
    oParams.firstChannel = 0;

    // create stream options
    RtAudio::StreamOptions options;

    // go for it
    try {
        // open a stream
        adac.openStream( &oParams, &iParams, MY_FORMAT, MY_SRATE,
                         &bufferFrames, &callme, (void *)&bufferBytes,
                         &options );
    }
    catch( RtError& e )
    {
        // error!
        cout << e.getMessage() << endl;
        exit( 1 );
    }

    // compute
    bufferBytes = bufferFrames * MY_CHANNELS * sizeof(SAMPLE);

    // test RtAudio functionality for reporting latency.
    cout << "stream latency: " << adac.getStreamLatency() << " frames" << endl;

    // go for it
    try {
        // start stream
        adac.startStream();

        // get input
        char input;
        std::cout << "running... press <enter> to quit (buffer frames: "
                  << bufferFrames << ")" << endl;
        std::cin.get(input);

        // stop the stream.
        adac.stopStream();
    }
    catch( RtError& e )
    {
        // print error message
        cout << e.getMessage() << endl;
        goto cleanup;
    }

cleanup:
    // close if open
    if( adac.isStreamOpen() )
        adac.closeStream();

    // done
    return 0;
}
